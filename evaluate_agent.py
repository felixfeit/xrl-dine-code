import numpy as np
from gym_swim_adapter.envs import SwimDiscreteActionsEnv

from decomposed_reward_deep_q_learning import DecomposedRewardDeepQLearningTrainer


def show_progress_bar(
        current_step,
        total_steps_exclusive,
        prefix='',
        max_number_of_updates=1000):
    total_steps = total_steps_exclusive - 1
    if total_steps > max_number_of_updates:  # avoid divison by zero
        if current_step % (total_steps // max_number_of_updates) != 0:
            return  # print at most max_number_of_updates updates

    percent = '{0:.2f}'.format(100 * (current_step / total_steps))
    filled_length = int(100 * current_step // total_steps)
    bar = '█' * filled_length + '-' * (100 - filled_length)
    print(f'\r{prefix} |{bar}| {percent}%', end='\r')
    if current_step == total_steps:  # new line on complete
        print()


def make_evaluation_environment(user_satisfaction_reward_weight=1.0):
    return SwimDiscreteActionsEnv(
        reward_type='decomposed_reward',
        user_satisfaction_reward_weight=user_satisfaction_reward_weight,
        store_simulation_results=False,
        seed=1,
        round_robin_trace_selection=True,
    )


def make_training_environment(user_satisfaction_reward_weight=1.0):
    return SwimDiscreteActionsEnv(
        reward_type='decomposed_reward',
        user_satisfaction_reward_weight=user_satisfaction_reward_weight,
        store_simulation_results=False,
    )


def evaluate_random_agent(num_steps=100_000, user_satisfaction_reward_weight=1.0):
    rewards = []
    evaluation_env = make_evaluation_environment(user_satisfaction_reward_weight)
    evaluation_env.reset()
    for i in range(num_steps):
        _, reward, _, _ = \
            evaluation_env.step(evaluation_env.action_space.sample())
        rewards.append(reward)
        show_progress_bar(i, num_steps, prefix='Random Agent')

    print(f'Random agent average reward over {num_steps} steps: '
          f'{np.mean(rewards):.5f}')


if __name__ == '__main__':
    user_satisfaction_weight = 1.0
    #evaluate_random_agent(num_steps=100_000, user_satisfaction_weight)

    training_env = make_training_environment(user_satisfaction_weight)
    trainer = DecomposedRewardDeepQLearningTrainer(
        continuing_env=training_env,
        learning_rate=0.0001,
        learning_starts=10_000,
        exploration_initial_eps=1.0,
        exploration_fraction=0.15,
        exploration_final_eps=0.05,
        batch_size=128,
        replay_memory_size=500_000,
    )
    trainer.train(200_000)

    evaluation_env = make_evaluation_environment(user_satisfaction_weight)
    trainer.evaluate(evaluation_env, num_steps=100_000)
