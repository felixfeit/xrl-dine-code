import json
from random import randint, random, sample, uniform
import numpy as np

from environmental_model import EnvironmentalRegressionModel


def generate_dummy_explanations(amount: int = 100, starting_timestep: int = 1):
    reward_semantics = ('Running Costs', 'Revenue', 'User Satisfaction')
    action_semantics = (
        'No Operation',
        'Add Server',
        'Remove Server',
        'Increase Dimmer',
        'Decrease Dimmer'
    )
    observation_semantics = (
        'basic_response_time',
        'opt_response_time',
        'basic_throughput',
        'opt_throughput',
        'avg_response_time',
        'utilization',
        'current_dimmer',
        'active_servers',
        'max_servers',
        'is_booting',
        'boot_remaining',
        'request_arrival_mean',
        'request_arrival_moving_mean',
        'request_arrival_variance',
        'request_arrival_moving_variance',
        'low_fidelity_service_time',
        'low_fidelity_service_time_variance',
        'service_time',
        'service_time_variance',
        'server_threads',
    )
    explanation_list = []
    for i in range(amount):
        explanation = Explanation(
            i + starting_timestep,
            {ose: random() * 10 for ose in observation_semantics},
            {rs: random() for rs in reward_semantics},
            sample(action_semantics, 1)[0],
            random() > 0.3,
            {ase: {rs: uniform(-2, 2) for rs in reward_semantics} for ase in action_semantics},
            {ase: {rs: uniform(-2, 2) for rs in reward_semantics} for ase in action_semantics},
            {rs: uniform(-2, 2) for rs in reward_semantics},
            {rs: uniform(-2, 2) for rs in reward_semantics}
        )

        if random() > 0.3:
            exp_str = 'To achieve the goal ' \
                      '<u><b>Higher User Satisfaction</b></u>, it ' \
                      'would currently be most important to choose ' \
                      '<u><b>Add Server</b></u>. ' \
                      'However, taking into account all the contrastive target dimensions, ' \
                      'I have chosen the action "Remove Server".\n\n' \
                      'More specifically, compared to the action ' \
                      '<b>"Add Server"</b>, the action ' \
                      '<b>"Remove Server"</b> has ' \
                      'a higher positive impact on the goal <b>"Lower Energy cost"</b>.'
            explanation.contrastive_important_actions_explanations = [
                (rs, sample(action_semantics, 1)[0], exp_str) for rs in sample(reward_semantics, randint(1, 2))
            ]

        explanation_list.append(explanation)

    return explanation_list


class Explanation:
    def __init__(
            self,
            timestep: int,
            current_state: dict[str, float],
            current_normalized_state: dict[str, float],
            last_rewards: dict[str, float],
            current_selected_action: str,
            is_greedy_action: bool,
            absolute_reward_channel_dominance: dict[str, dict[str, float]],
            relative_reward_channel_dominance: dict[str, dict[str, float]],
            current_state_value: dict[str, float],
            min_next_state_value: dict[str, float],
            max_next_state_value: dict[str, float],
            contrastive_important_actions_explanations: list[tuple[str, str, str]] = None) -> None:
        self.timestep: int = timestep
        self.current_state: dict[str, float] = current_state
        self.current_normalized_state: dict[str, float] = current_normalized_state
        self.last_rewards: dict[str, float] = last_rewards
        self.current_selected_action: str = current_selected_action
        self.is_greedy_action: bool = is_greedy_action
        self.absolute_reward_channel_dominance: dict[str, dict[str, float]] = \
            absolute_reward_channel_dominance
        self.relative_reward_channel_dominance: dict[str, dict[str, float]] = \
            relative_reward_channel_dominance
        self.current_state_value: dict[str, float] = current_state_value
        self.min_next_state_value: dict[str, float] = min_next_state_value
        self.max_next_state_value: dict[str, float] = max_next_state_value
        self.contrastive_important_actions_explanations: list[tuple[str, str, str]] = \
            contrastive_important_actions_explanations if contrastive_important_actions_explanations is not None else []

    def __str__(self) -> str:
        explanation_string = 'Action Type: '
        explanation_string += 'Exploiting' if self.is_greedy_action else 'Exploring'
        explanation_string += '\n\n'
        
        for _, _, expl in self.contrastive_important_actions_explanations:
            explanation_string += expl
            explanation_string += '\n\n'

        return explanation_string
    
    @classmethod
    def from_dict(cls, explanation_dict):
        return Explanation(
            timestep=explanation_dict['timestep'],
            current_state=explanation_dict['current_state'],
            current_normalized_state=explanation_dict['current_normalized_state'],
            last_rewards=explanation_dict['last_rewards'],
            current_selected_action=explanation_dict['current_selected_action'],
            is_greedy_action=explanation_dict['timestep'],
            absolute_reward_channel_dominance= \
                explanation_dict['absolute_reward_channel_dominance'],
            relative_reward_channel_dominance= \
                explanation_dict['relative_reward_channel_dominance'],
            current_state_value=explanation_dict['current_state_value'],
            min_next_state_value=explanation_dict['min_next_state_value'],
            max_next_state_value=explanation_dict['max_next_state_value'],
            contrastive_important_actions_explanations= \
                explanation_dict['contrastive_important_actions_explanations'],
        )

    def to_dict(self):
        return {
            'timestep': self.timestep,
            'current_state': self.current_state,
            'current_normalized_state': self.current_normalized_state,
            'last_rewards': self.last_rewards,
            'current_selected_action': self.current_selected_action,
            'is_greedy_action': self.is_greedy_action,
            'absolute_reward_channel_dominance': self.absolute_reward_channel_dominance,
            'relative_reward_channel_dominance': self.relative_reward_channel_dominance,
            'current_state_value': self.current_state_value,
            'min_next_state_value': self.min_next_state_value,
            'max_next_state_value': self.max_next_state_value,
            'contrastive_important_actions_explanations': self.contrastive_important_actions_explanations,
        }


if __name__ == '__main__':
    explanations = generate_dummy_explanations(150)
    with open('./explanations/explanations.json', 'w', encoding='utf-8') as f:
        json.dump([e.to_dict() for e in explanations], f, ensure_ascii=False, indent=4)
