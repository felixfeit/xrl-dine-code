import numpy as np


class IQRAnalyzer:
    @staticmethod
    def _get_median(sorted_values):
        if len(sorted_values) % 2 == 0:
            return (sorted_values[len(sorted_values) // 2 - 1] + sorted_values[len(sorted_values) // 2]) / 2
        else:
            return sorted_values[len(sorted_values) // 2]

    @staticmethod
    def _get_quartiles(values):
        sorted_values = sorted(values)
        q2 = IQRAnalyzer._get_median(sorted_values)
        half = len(sorted_values) // 2
        if len(sorted_values) % 2 == 0:
            q1 = IQRAnalyzer._get_median(sorted_values[:half])
            q3 = IQRAnalyzer._get_median(sorted_values[half:])
        else:
            q1 = IQRAnalyzer._get_median(sorted_values[:half])
            q3 = IQRAnalyzer._get_median(sorted_values[half + 1:])
        return q1, q2, q3
    
    @staticmethod
    def get_bottom_outlier_indices(values, iqr_factor=0.2):
        q1, _, q3 = IQRAnalyzer._get_quartiles(values)
        iqr = q3 - q1
        threshold = q1 - iqr_factor * iqr
        return [i for i, v in enumerate(values) if v < threshold]
    
    @staticmethod
    def get_top_outlier_indices(values, iqr_factor=0.2):
        q1, _, q3 = IQRAnalyzer._get_quartiles(values)
        iqr = q3 - q1
        threshold = q3 + iqr_factor * iqr
        return [i for i, v in enumerate(values) if v > threshold]

    @staticmethod
    def get_outliers(values, iqr_factor=0.2):
        return \
            IQRAnalyzer.get_bottom_outlier_indices(values, iqr_factor), \
            IQRAnalyzer.get_top_outlier_indices(values, iqr_factor)
    
    @staticmethod
    def get_strongest_top_outlier_index(values, iqr_factor=0.2):
        outliers_indices = IQRAnalyzer.get_top_outlier_indices(values, iqr_factor)
        if not outliers_indices:
            return None
        
        strongest_outlier_index = outliers_indices[0]
        for oi in outliers_indices[1:]:
            if values[oi] > values[strongest_outlier_index]:
                strongest_outlier_index = oi
        return strongest_outlier_index


class EvennnessAnalyzer:
    @staticmethod
    def _shift_above_zero(values):  # range: 0...1
        return [v - min(values) for v in values] if min(values) < 0 else values
    
    @staticmethod
    def _get_probability_distribution(values):
        return [v / sum(values) for v in values]

    @staticmethod
    def _calculate_eveneness_of_values(values):
        shifted = EvennnessAnalyzer._shift_above_zero(values)
        distribution = EvennnessAnalyzer._get_probability_distribution(shifted)
        return -sum([d * np.log(d) / np.log(len(distribution)) for d in distribution if d > 0])

    @staticmethod
    def get_strongest_top_outlier_index(values, threshold=0.9):
        values_uneven = \
            EvennnessAnalyzer._calculate_eveneness_of_values(values) < threshold
        if not values_uneven:
            return None
        values_without_max = sorted(values)[:-1]
        values_still_uneven = \
            EvennnessAnalyzer._calculate_eveneness_of_values(values_without_max) < threshold
        return None if values_still_uneven else np.argmax(values)
