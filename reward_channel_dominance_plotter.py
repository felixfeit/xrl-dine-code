import matplotlib.pyplot as plt
import numpy as np
from copy import copy
import time


class RewardChannelDominancePlotter:
    def __init__(
            self,
            action_semantics: tuple[str, ...],
            reward_channel_semantics: tuple[str, ...]) -> None:
        self.action_semantics = action_semantics
        self.reward_channel_semantics = reward_channel_semantics

    @staticmethod
    def calculate_bottom(action_values_per_channel, channel_index: int):
        num_actions = len(action_values_per_channel[0])
        bottom = [0] * num_actions
        if channel_index == 0:
            return bottom

        action_values_until_channel = copy(action_values_per_channel)[:channel_index]
        channel_action_values = copy(action_values_per_channel)[channel_index]
        for action_values in action_values_until_channel:
            for action_index in range(num_actions):
                if np.sign(channel_action_values[action_index]) == np.sign(action_values[action_index]):
                    bottom[action_index] += action_values[action_index]

        return bottom

    def plot_relative_reward_channel_dominance_all_actions(self, channel_values):
        relative_channel_values = []
        for i in range(len(self.reward_channel_semantics)):
            relative_channel_values.append(
                [channel_values[i][j] - min(channel_values[i]) for j in range(len(self.action_semantics))]
            )

        self.plot_absolute_reward_channel_dominance_all_actions(relative_channel_values, label='Relative Dominance Value')

    def plot_absolute_reward_channel_dominance_all_actions(self, action_values_per_channel, label='Absolute Dominance Value'):
        assert len(action_values_per_channel) == len(self.reward_channel_semantics)
        assert len(action_values_per_channel[0]) == len(self.action_semantics)

        _, ax = plt.subplots()

        for i, reward_channel_semantic in enumerate(self.reward_channel_semantics):
            ax.bar(
                [s.replace(' ', '\n') for s in self.action_semantics],
                action_values_per_channel[i],
                width=0.45,
                label=reward_channel_semantic,
                bottom=RewardChannelDominancePlotter.calculate_bottom(action_values_per_channel, i),
                edgecolor='black',
            )

        ax.axhline(y=0, color='k')
        ax.set_ylabel(label)
        ax.legend()
        plt.savefig(str(time.time_ns()) + '.pdf')
