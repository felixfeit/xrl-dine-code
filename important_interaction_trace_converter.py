from pathlib import Path
import json

from explanation import Explanation
import important_interactions
from reward_channel_dominance_calculator import RewardChannelDominanceCalculator
from reward_decomposition_explanations import RewardDecompositionNaturalLanguageExplainer


def get_contrastive_important_actions(
        channel_values,
        selected_action,
        get_strongest_top_outlier_index,
        threshold) -> list[tuple[int, int]]:
    important_actions = [get_strongest_top_outlier_index(av, threshold)
                        for av in channel_values]
    contrastive_important_actions = []
    for i, ia in enumerate(important_actions):
        if ia is not None and ia != selected_action:
            contrastive_important_actions.append((i, ia))

    return contrastive_important_actions


def get_contrastive_important_actions_explanations(
        channel_values, 
        selected_action, 
        differing_important_actions,
        get_constrastive_important_action_explanation,
        reward_channel_semantics,
        action_semantics):
    contrastive_important_actions_explanations = []
    for dia in differing_important_actions:
        ce = get_constrastive_important_action_explanation(
            channel_values,
            selected_action,
            dia[0],
            dia[1]
        )
        contrastive_important_actions_explanations.append((
            reward_channel_semantics[dia[0]],
            action_semantics[dia[1]],
            ce
        )) 
    return contrastive_important_actions_explanations


def convert(in_file_name, get_strongest_top_outlier_index, threshold, out_file_name=None):
    num_ie_explanations = 0
    out_file_name = in_file_name if out_file_name is None else out_file_name
    in_file = Path(in_file_name)
    out_file = Path(out_file_name)
    out_file.parent.mkdir(parents=True, exist_ok=True)

    explanations: list[Explanation] = []
    if in_file.is_file():
        with open(in_file, 'r') as f:
            raw_explanations = json.load(f)
            explanations.extend([Explanation.from_dict(d) for d in raw_explanations])
    
    rdn: RewardDecompositionNaturalLanguageExplainer = None
    rcdc: RewardChannelDominanceCalculator = None
    transformed_explanations = []
    for explanation in explanations:
        if rdn is None:
            rdn = RewardDecompositionNaturalLanguageExplainer(
                list(explanation.current_state.keys()),
                list(explanation.absolute_reward_channel_dominance.keys()),
                list(explanation.last_rewards.keys())
            )
        
        if rcdc is None:
            rcdc = RewardChannelDominanceCalculator(
                list(explanation.absolute_reward_channel_dominance.keys()),
                list(explanation.last_rewards.keys())
            )

        differing_important_actions = get_contrastive_important_actions(
            rcdc.get_channel_values(explanation.absolute_reward_channel_dominance),
            rdn.action_semantics.index(explanation.current_selected_action),
            get_strongest_top_outlier_index,
            threshold
        )

        contrastive_important_actions_explanations = get_contrastive_important_actions_explanations(
            rcdc.get_channel_values(explanation.absolute_reward_channel_dominance),
            rdn.action_semantics.index(explanation.current_selected_action),
            differing_important_actions,
            rdn.get_constrastive_important_action_explanation,
            rdn.reward_channel_semantics,
            rdn.action_semantics
        )

        transformed_explanation = explanation.to_dict()
        transformed_explanation['contrastive_important_actions_explanations'] = \
            contrastive_important_actions_explanations
        transformed_explanations.append(transformed_explanation)
        num_ie_explanations += len(differing_important_actions)

    with open(out_file, 'w', encoding='utf-8') as f:
        json.dump(
            [e for e in transformed_explanations],
            f,
            ensure_ascii=False,
            indent=4
        )
    
    return num_ie_explanations


def convert_to_iqr(in_file_name, out_file_name=None, threshold=0.2):
    return convert(
        in_file_name,
        important_interactions.IQRAnalyzer.get_strongest_top_outlier_index,
        threshold,
        out_file_name,
    )


def convert_to_evenness(in_file_name, out_file_name=None, threshold=0.9):
    return convert(
        in_file_name,
        important_interactions.EvennnessAnalyzer.get_strongest_top_outlier_index,
        threshold,
        out_file_name,
    )

def convert_iqr_until(at_most=30, at_least=5, starting_threshold=0.3, step=0.05):
    current_threshold = starting_threshold
    while True:
        number_of_explanations = convert_to_iqr(
            str(Path(__file__).parent.resolve()) + '/dashboard/build/data/explanations.json',
            str(Path(__file__).parent.resolve()) + '/dashboard/build/data/explanations.json',
            threshold=current_threshold
        )
        
        print(current_threshold, number_of_explanations)
        if number_of_explanations > at_most:
            current_threshold += step
        elif number_of_explanations < at_least:
            current_threshold -= step
        else:
            return number_of_explanations


if __name__ == '__main__':
    convert_iqr_until(
        at_most=100,
        at_least=10,
        starting_threshold=0.3,
        step=0.01
    )
