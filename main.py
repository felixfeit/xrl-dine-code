import pathlib
import time
from itertools import count

from gym_swim_adapter.envs import SwimDiscreteActionsEnv

from decomposed_interestingness_elements_engine import DecomposedInterestingnessElementsEngine
from decomposed_reward_deep_q_learning import DecomposedRewardDeepQLearningTrainer

if __name__ == '__main__':
    non_costs_reward_factor = 1.0
    env = SwimDiscreteActionsEnv(
        reward_type='decomposed_reward',
        store_simulation_results=False,
        user_satisfaction_reward_weight=non_costs_reward_factor * 4,
        revenue_reward_weight=non_costs_reward_factor * 2,
        illegal_action_punishment=-0.1,
        any_action_punishment=-0.1,
        least_satisfactory_response_time_threshold=0.1,
        most_satisfactory_response_time_threshold=0.02,
        revenue_request_optional_content_factor=5.0,
        round_robin_trace_selection=False,
    )
    trainer = DecomposedRewardDeepQLearningTrainer(
        continuing_env=env,
        learning_rate=0.0001,
        learning_starts=5_000,
        exploration_initial_eps=0.3,
        exploration_fraction=0.5,
        exploration_final_eps=0.05,
        gamma=0.96,
        batch_size=64,
        gradient_steps=3,
        replay_memory_size=50_000,
    )

    run_id = f'satisfaction_weight_{env._user_satisfaction_reward_weight}' \
             f'_revenue_weight_{env._revenue_reward_weight}' \
             f'_illegal_punishment_{env._illegal_action_punishment}' \
             f'_any_punishment_{env._any_action_punishment}' \
             f'_lower_response_time_{env._least_satisfactory_response_time_threshold}' \
             f'_upper_response_time_{env._most_satisfactory_response_time_threshold}' \
             f'_optional_content_factor_{env._revenue_request_optional_content_factor}' \
             f'_hidden_layers_{"_".join([str(l) for l in trainer.hidden_layers])}' \
             f'_activation_{trainer.activation_function_name.lower()}'
    run_id = run_id.replace('.', '_')
    model_file_name = 'model_' + run_id + '.tar'
    if pathlib.Path(model_file_name).is_file():
        trainer.restore_from_file(model_file_name)

    # initial training
    trainer.train(50_000)
    trainer.store_to_file(model_file_name)

    explainer = DecomposedInterestingnessElementsEngine(
        env.observation_semantics,
        env.action_semantics,
        env.get_reward_channel_semantics(),
        trainer.global_replay_buffer,
        trainer.sub_agents,
        DecomposedInterestingnessElementsEngine.Configuration(
            train_model_starts=0,
            train_model_initial_epochs=30_000,
        ),
        explanations_running_out_file=str(pathlib.Path(__file__).parent.resolve()) + '/dashboard/build/data/explanations.json',
        # num_sleep_seconds_per_action=10,
        # start_sleep_after_interaction=250
    )

    explanations_run_id = \
        time.strftime('%Y_%m_%d_%H_%M_%S_') + \
        f'satisfaction_weight_{env._user_satisfaction_reward_weight}' \
        f'_revenue_weight_{env._revenue_reward_weight}' \
        f'_illegal_punishment_{env._illegal_action_punishment}' \
        f'_any_punishment_{env._any_action_punishment}' \
        f'_lower_response_time_{env._least_satisfactory_response_time_threshold}' \
        f'_upper_response_time_{env._most_satisfactory_response_time_threshold}' \
        f'_optional_content_factor_{env._revenue_request_optional_content_factor}'
    for i in count(2):
        # explanation phase
        trainer.explainer_callback = explainer.report_new_interaction
        trainer.exploration_initial_eps = 0.00
        trainer.exploration_final_eps = 0.00
        trainer.train(500)
        explainer.save_checkpoint(str(pathlib.Path(__file__).parent.resolve()) + f'/explanations/{explanations_run_id}/iteration_{i:04}.json')

        # training phase
        trainer.explainer_callback = None
        trainer.exploration_initial_eps = 0.3
        trainer.exploration_final_eps = 0.05
        trainer.train(20_000)
        trainer.store_to_file(model_file_name)
