# update rule taken from "Explainable reinforcement learning via reward decomposition"
import time
from collections import deque
from random import sample, random
from typing import Callable, Optional

import gym
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import wandb
from gym_swim_adapter.envs import SwimDiscreteActionsEnv
from torch import Tensor, LongTensor
from torch.utils.tensorboard import SummaryWriter


class ReplayMemory(deque):
    def __init__(self, max_length: Optional[int]) -> None:
        super().__init__(iterable=[], maxlen=max_length)

    def sample(self, num_samples: int) -> list:
        return sample(self, num_samples) \
            if len(self) > num_samples else list(self)


class DQN(nn.Module):
    def __init__(
            self,
            num_observation_variables: int,
            num_discrete_actions: int,
            hidden_layers: tuple[int, ...] = (128, 128, 64),
            activation_function_name: str = 'ReLU') -> None:
        super().__init__()

        activation = getattr(nn, activation_function_name)
        layers = (
            num_observation_variables,
            *hidden_layers,
            num_discrete_actions
        )

        network_architecture = []
        for i in range(1, len(layers)):
            network_architecture.append(nn.Linear(layers[i - 1], layers[i]))
            if i < len(layers) - 1:
                network_architecture.append(activation())

        self.model = nn.Sequential(*network_architecture)

    def forward(self, x):
        return self.model(x)


class DecomposedRewardDeepQLearningTrainer:
    class DoubleDQNSubAgent:
        def __init__(
                self,
                num_observation_dimensions: int,
                num_actions: int,
                global_replay_memory: ReplayMemory,
                get_overall_greedy_action: Callable[[Tensor], int],
                hidden_layers: tuple[int, ...] = (128, 128, 64),
                activation_function_name: str = 'ReLU',
                learning_starts: int = 50_000,
                batch_size: int = 32,
                gradient_steps: int = 4,
                gamma: float = 0.99,
                learning_rate: float = 0.001,
                target_update_interval: int = 10000) -> None:
            self.policy_network = DQN(
                num_observation_dimensions,
                num_actions,
                hidden_layers,
                activation_function_name
            )
            self.target_network = DQN(
                num_observation_dimensions,
                num_actions,
                hidden_layers,
                activation_function_name
            )
            self._load_policy_weights_into_target_network()
            self._global_replay_memory = global_replay_memory
            self._get_overall_greedy_action = get_overall_greedy_action
            self._batch_size = batch_size
            self._gradient_steps = gradient_steps
            self._learning_starts = learning_starts
            self._gamma = gamma
            self.optimizer = \
                optim.Adam(self.policy_network.parameters(), lr=learning_rate)
            self._target_update_interval = target_update_interval

        def on_step(self, timestep: int, agent_id: int) -> Optional[float]:
            loss = None
            if timestep > self._learning_starts:
                losses = [self._optimize_model(agent_id) for _ in range(self._gradient_steps)]
                loss = np.mean(losses)
                if timestep % self._target_update_interval == 0:
                    self._load_policy_weights_into_target_network()

            return loss

        def _load_policy_weights_into_target_network(self):
            self.target_network.load_state_dict(self.policy_network.state_dict())

        def get_action_values_tensor(self, states: Tensor) -> Tensor:
            return self.policy_network(states)

        def get_action_values(self, state):
            return self.policy_network(Tensor(np.array([state])))[0].detach().numpy()

        def _optimize_model(self, agent_id: int, loss_fn=nn.MSELoss()):
            batch = self._global_replay_memory.sample(self._batch_size)
            states, actions, decomposed_rewards, next_states = zip(*batch)
            rewards = [dr[agent_id] for dr in decomposed_rewards]
            states = Tensor(np.array(states))
            actions = LongTensor(actions)
            rewards = Tensor(rewards)
            next_states = Tensor(np.array(next_states))

            all_q_values = self.policy_network(states)
            selected_action_q_values = \
                self._get_values_of_actions(all_q_values, actions)

            # The following line implements reward decomposition according to
            # Juozapaitis et al.'s paper "Explainable Reinforcement Learning
            # via Reward Decomposition" (2019). The idea is to optimize the
            # policy of the sub-agent towards the overall (=aggregated) policy
            # instead of its own policy.
            next_max_actions_overall_policy = \
                self._get_overall_greedy_action(next_states)

            # From here on unmodified double DQN continues
            next_all_q_values_target_net = self.target_network(next_states)
            next_selected_q_values_target_net = self._get_values_of_actions(
                next_all_q_values_target_net,
                next_max_actions_overall_policy
            )
            expected_q_values = \
                rewards + self._gamma * next_selected_q_values_target_net

            loss = loss_fn(selected_action_q_values, expected_q_values)
            self.optimizer.zero_grad()
            loss.backward()
            self.optimizer.step()

            return float(loss)

        @staticmethod
        def _get_values_of_actions(values, actions):
            return values.gather(1, actions.unsqueeze(1)).squeeze(1)

    def __init__(
            self,
            continuing_env: gym.Env,
            hidden_layers: tuple[int, ...] = (128, 128, 64),
            activation_function_name: str = 'ReLU',
            replay_memory_size: Optional[int] = 1_000_000,
            learning_starts: int = 50_000,
            batch_size: int = 32,
            gradient_steps: int = 4,
            gamma: float = 0.99,
            learning_rate: float = 0.001,
            target_update_interval: int = 10000,
            exploration_fraction: float = 0.1,
            exploration_initial_eps: float = 1.0,
            exploration_final_eps: float = 0.05,
            explainer_callback: Optional[Callable[[list[float], list[float], int], None]] = None,
            tensorboard_dir: str = 'tensorboard_logs') -> None:
        self._env = continuing_env
        self.hidden_layers = hidden_layers
        self.activation_function_name = activation_function_name
        self.learning_starts = learning_starts
        self.batch_size = batch_size
        self.gradient_steps = gradient_steps
        self.gamma = gamma
        self.learning_rate = learning_rate
        self.target_update_interval = target_update_interval
        self.exploration_fraction = exploration_fraction
        self.exploration_initial_eps = exploration_initial_eps
        self.exploration_final_eps = exploration_final_eps
        self.explainer_callback = explainer_callback
        self.global_replay_buffer = ReplayMemory(replay_memory_size)
        self._total_training_step = 0

        self._initialize_wandb()
        self._initialize_tensorboard(tensorboard_dir)
        self._initialize_sub_agents()

    def _get_config(self):
        return {
            **{
                'hidden_layers': self.hidden_layers,
                'activation_function_name': self.activation_function_name,
                'learning_starts': self.learning_starts,
                'batch_size': self.batch_size,
                'gamma': self.gamma,
                'target_update_interval': self.target_update_interval,
                'exploration_fraction': self.exploration_fraction,
                'exploration_initial_eps': self.exploration_initial_eps,
                'exploration_final_eps': self.exploration_final_eps,
            },
            **self._env.get_config()
        }

    def _initialize_wandb(self):
        wandb.login(key='TODO: INSERT')
        wandb.init(
            project='TODO: INSERT',
            entity='TODO: INSERT',
            config=self._get_config()
        )

    def _initialize_tensorboard(self, dir):
        self._tensorboard = SummaryWriter(
            dir,
            filename_suffix='decomposed_dqn'
        )

    def _initialize_sub_agents(self):
        self.sub_agents: list[DecomposedRewardDeepQLearningTrainer.DoubleDQNSubAgent] = []
        for _ in self._env.get_reward_channel_semantics():
            sub_agent = self.DoubleDQNSubAgent(
                num_observation_dimensions=self._env.observation_space.shape[0],
                num_actions=self._env.action_space.n,
                global_replay_memory=self.global_replay_buffer,
                get_overall_greedy_action=self._get_greedy_actions_of_aggregated_policy_tensor,
                hidden_layers=self.hidden_layers,
                activation_function_name=self.activation_function_name,
                learning_starts=self.learning_starts,
                batch_size=self.batch_size,
                gradient_steps=self.gradient_steps,
                gamma=self.gamma,
                learning_rate=self.learning_rate,
                target_update_interval=self.target_update_interval,
            )
            self.sub_agents.append(sub_agent)

    @staticmethod
    def _show_progress_bar(
            current_step,
            total_steps_exclusive,
            prefix='',
            postfix='',
            max_number_of_updates=1000):
        total_steps = total_steps_exclusive - 1
        update_each_step = max(total_steps // max_number_of_updates, 1)
        if current_step % update_each_step != 0:
            return  # print at most max_number_of_updates updates

        percent = '{0:.2f}'.format(100 * (current_step / total_steps))
        filled_length = int(100 * current_step // total_steps)
        bar = '█' * filled_length + '-' * (100 - filled_length)
        print(f'\r{prefix} |{bar}| {percent}% {postfix}', end='\r')
        if current_step == total_steps:  # new line on complete
            print()

    def _log_to_tensorboard(self, current_state, action,
                            is_exploratory_action, observation,
                            decomposed_reward, eps):
        action_semantics = self._env.action_semantics
        is_useless_action = self._env.is_useless_action_raw_state(current_state, action)
        action = action_semantics.index('No Operation') if is_useless_action else action
        for ac in action_semantics:
            is_action = 1 if not is_exploratory_action and ac == action_semantics[action] else 0
            self._tensorboard.add_scalar(
                f'Actions/{ac}',
                is_action,
                self._total_training_step
            )
            wandb.log({f'Actions/{ac}': is_action}, step=self._total_training_step)

        self._tensorboard.add_scalar(
            f'Actions/Exploratory Action',
            1 if is_exploratory_action else 0,
            self._total_training_step
        )
        self._tensorboard.add_scalar(
            f'Actions/Illegal Action',
            1 if not is_exploratory_action and is_useless_action else 0,
            self._total_training_step
        )
        self._tensorboard.add_scalar(
            f'RL/Aggregated Reward',
            sum(decomposed_reward),
            self._total_training_step
        )
        self._tensorboard.add_scalar('RL/Epsilon', eps, self._total_training_step)
        wandb.log({
            'Actions/Exploratory Action': 1 if is_exploratory_action else 0,
            'Actions/Illegal Action': 1 if not is_exploratory_action and is_useless_action else 0,
            'RL/Aggregated Reward': sum(decomposed_reward),
            'RL/Epsilon': eps,
        }, step=self._total_training_step)

        for i in range(len(self.sub_agents)):
            self._tensorboard.add_scalar(
                f'Reward Channels/{self._env.get_reward_channel_semantics()[i]}',
                decomposed_reward[i],
                self._total_training_step
            )
            wandb.log({
                f'Reward Channels/{self._env.get_reward_channel_semantics()[i]}': decomposed_reward[i]
            }, step=self._total_training_step)

        all_state_variables = list(self._env.ALL_OBSERVATION_VARIABLES.keys())
        for i, state_variable in enumerate(all_state_variables):
            self._tensorboard.add_scalar(
                f'State/{state_variable}',
                observation[i],
                self._total_training_step
            )
            wandb.log({f'State/{state_variable}': observation[i]}, step=self._total_training_step)

        avg_server_utilization = \
            1 - ((observation[all_state_variables.index('active_servers')] - observation[all_state_variables.index('utilization')]) / observation[all_state_variables.index('active_servers')])
        self._tensorboard.add_scalar(
            'State/Average Server Utilization',
            avg_server_utilization,
            self._total_training_step
        )
        wandb.log({
            'State/Average Server Utilization': avg_server_utilization
        }, step=self._total_training_step)

    def _log_loss_to_tensorboard(self, loss, agent_index):
        if loss is not None:
            self._tensorboard.add_scalar(
                f'Loss/{self._env.get_reward_channel_semantics()[agent_index]}',
                loss,
                self._total_training_step
            )
            wandb.log({
                f'Loss/{self._env.get_reward_channel_semantics()[agent_index]}': loss
            }, step=self._total_training_step)

    def _act(self, step, num_steps, state):
        self._show_progress_bar(step, num_steps, 'Training', postfix=step)
        current_eps = self._get_current_epsilon(step, num_steps)
        action, is_exploratory_action = \
            self._get_epsilon_greedy_action(state, current_eps)
        next_state, decomposed_reward, _, _ = self._env.step(action)

        if self.explainer_callback is not None:
            self.explainer_callback(state, decomposed_reward, action)

        self._log_to_tensorboard(
            state,
            action,
            is_exploratory_action,
            next_state,
            decomposed_reward,
            current_eps
        )
        return next_state, action, decomposed_reward

    def _add_new_sample_and_learn(self, state, action, decomposed_reward,
                                  next_state) -> None:
        self.global_replay_buffer.append((
            state,
            action,
            decomposed_reward,
            next_state
        ))
        for i, sa in enumerate(self.sub_agents):
            loss = sa.on_step(self._total_training_step, agent_id=i)
            self._log_loss_to_tensorboard(loss, i)

    def _step(self, step, num_steps, current_state):
        step_start_time = time.time()
        next_state, action, decomposed_reward = \
            self._act(step, num_steps, current_state)
        self._add_new_sample_and_learn(
            current_state,
            action,
            decomposed_reward,
            next_state
        )
        self._log_step_duration(step_start_time)
        return next_state

    def _log_step_duration(self, step_start_time):
        duration = time.time() - step_start_time
        self._tensorboard.add_scalar(
            'RL/Step Duration in Seconds',
            duration,
            self._total_training_step
        )
        wandb.log({
            f'RL/Step Duration in Seconds': duration
        }, step=self._total_training_step)

    def store_to_file(self, path: str):
        reward_semantics = self._env.get_reward_channel_semantics()
        bundle = {}
        for i, rs in enumerate(reward_semantics):
            bundle['policy_network_' + rs] = self.sub_agents[i].policy_network.state_dict()
            bundle['target_network_' + rs] = self.sub_agents[i].target_network.state_dict()
            bundle['optimizer_' + rs] = self.sub_agents[i].optimizer.state_dict()
        bundle['replay_buffer'] = list(self.global_replay_buffer)
        bundle['total_training_step'] = self._total_training_step
        torch.save(bundle, path)
    
    def restore_from_file(self, path: str):
        reward_semantics = self._env.get_reward_channel_semantics() 
        bundle = torch.load(path)
        for i, rs in enumerate(reward_semantics):
            self.sub_agents[i].policy_network.load_state_dict(bundle['policy_network_' + rs])
            self.sub_agents[i].policy_network.train()
            self.sub_agents[i].target_network.load_state_dict(bundle['target_network_' + rs])
            self.sub_agents[i].target_network.train()
            self.sub_agents[i].optimizer.load_state_dict(bundle['optimizer_' + rs])
        self.global_replay_buffer.extend(bundle['replay_buffer'])
        self._total_training_step = bundle['total_training_step']

    def train(self, num_steps=1_000_000):
        state = self._env.reset()
        for step in range(num_steps):
            self._total_training_step += 1
            state = self._step(step, num_steps, state)

    def evaluate(self, evaluation_env=None, num_steps=100_000):
        if evaluation_env is None:
            evaluation_env = self._env

        rewards = []
        state = evaluation_env.reset()
        for i in range(num_steps):
            self._show_progress_bar(i, num_steps, 'Evaluation')
            action = self._get_greedy_action_of_aggregated_policy(state)
            state, reward, _, _ = evaluation_env.step(action)
            rewards.append(reward)

        print(f'Average reward after {num_steps} greedy steps: {np.mean(rewards)}')

    def _get_greedy_actions_of_aggregated_policy_tensor(self, states: Tensor):
        q_values = \
            [sa.get_action_values_tensor(states) for sa in self.sub_agents]
        accumulated_q_values = sum(q_values)
        return accumulated_q_values.argmax(1)

    def _get_greedy_action_of_aggregated_policy(self, state: np.ndarray) -> int:
        greedy_action_tensor = \
            self._get_greedy_actions_of_aggregated_policy_tensor(Tensor(np.array([state])))
        return int(greedy_action_tensor[0])

    def _get_epsilon_greedy_action(self, state, current_eps) -> tuple[int, bool]:
        is_exploratory_action = random() < current_eps 
        if is_exploratory_action:
            action = self._env.action_space.sample()
        else:
            action = self._get_greedy_action_of_aggregated_policy(state)
        return action, is_exploratory_action

    def _get_current_epsilon(self, current_step, num_steps):
        initial_eps = self.exploration_initial_eps
        final_eps = self.exploration_final_eps
        current_exploration_fraction = \
            (current_step / (num_steps * self.exploration_fraction))
        exploration_range = initial_eps - final_eps
        return max(
            initial_eps - exploration_range * current_exploration_fraction,
            final_eps
        )


if __name__ == '__main__':
    env = SwimDiscreteActionsEnv(
        reward_type='decomposed_reward',
        store_simulation_results=False
    )
    trainer = DecomposedRewardDeepQLearningTrainer(
        env,
        learning_rate=0.0001,
        learning_starts=10000,
        exploration_fraction=0.5,
        exploration_final_eps=0.05,
        batch_size=128,
        replay_memory_size=None,
    )

    try:
        trainer.train(500_000)
    except Exception as e:
        print(str(e))
    finally:  # save replay buffer for training of environmental model
        import pickle
        with open(f'replay_buffer_{time.time_ns()}.pkl', 'wb') as file:
            pickle.dump(list(trainer.global_replay_buffer), file)
