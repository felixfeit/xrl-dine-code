import pickle
import random
import time
from typing import Optional

import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler


def one_hot_encode(action_list, num_actions):
    actions = np.array(action_list)
    res = np.eye(num_actions)[np.array(actions).reshape(-1)]
    return res.reshape(list(actions.shape) + [num_actions])


def states_actions_to_unnormalized_input(states, actions, num_actions):
    oh_actions = one_hot_encode(actions, num_actions)
    samples = np.concatenate((states, oh_actions), axis=1)
    return samples


class RegressionModel(nn.Module):
    def __init__(
            self,
            number_inputs: int,
            number_outputs: int,
            hidden_layers: tuple[int, ...] = (32, 64, 32),
            activation_function_name: str = 'ReLU',
            model_path: Optional[str] = None) -> None:
        super().__init__()
        self.model = self._build_model(
            number_inputs,
            number_outputs,
            hidden_layers,
            activation_function_name,
        )
        if model_path:
            self.load_state_dict(torch.load(model_path))

    def forward(self, sample):
        return self.model(sample)

    @staticmethod
    def _build_model(
            number_inputs,
            number_outputs,
            hidden_layers,
            activation_function_name):
        activation = getattr(nn, activation_function_name)
        layers = (
            number_inputs,
            *hidden_layers,
            number_outputs,
        )

        network_architecture = []
        for i in range(1, len(layers)):
            network_architecture.append(nn.Linear(layers[i - 1], layers[i]))
            if i < len(layers) - 1:
                network_architecture.append(activation())

        return nn.Sequential(*network_architecture)


class EnvironmentalRegressionModel(RegressionModel):
    def __init__(
            self,
            number_inputs: int,
            number_outputs: int,
            feature_means: list[float],
            feature_stds: list[float],
            number_actions: int,
            hidden_layers: tuple[int, ...] = (32, 64, 32),
            activation_function_name: str = 'ReLU',
            model_path: Optional[str] = None) -> None:
        super().__init__(
            number_inputs,
            number_outputs,
            hidden_layers=hidden_layers,
            activation_function_name=activation_function_name,
            model_path=model_path)
        self.feature_means = np.array(feature_means)
        self.feature_stds = np.array(feature_stds)
        self.number_actions = number_actions

    def normalize_state(self, state: np.ndarray) -> np.ndarray:
        stds = np.copy(self.feature_stds[:len(state)])
        # force normalization to be zero when std == 0
        stds[self.feature_stds[:len(state)] == 0.0] = np.inf
        return (state - self.feature_means[:len(state)]) / stds  # z-score

    def normalize_inputs(self, samples: np.ndarray) -> np.ndarray:
        stds = np.copy(self.feature_stds)
        # force normalization to be zero when std == 0
        stds[self.feature_stds == 0.0] = np.inf
        return (samples - self.feature_means) / stds  # z-score

    def infer_next_state(self, state: np.ndarray, action: int):
        X_unnormalized = states_actions_to_unnormalized_input(
            [state],
            [action],
            self.number_actions
        )
        X = self.normalize_inputs(X_unnormalized)
        return self.forward(torch.Tensor(X)).detach().numpy()[0]


class EnvironmentalModelTrainer:
    replay_buffer_type = list[tuple[
        tuple[float, ...],      # state
        int,                    # discrete action
        tuple[float, ...],      # decomposed reward
        tuple[float, ...]       # next state
    ]]

    def __init__(
            self,
            num_state_variables: int,
            num_discrete_actions: int) -> None:
        self.num_state_variables = num_state_variables
        self.num_discrete_actions = num_discrete_actions
        self.model = None

    def train_from_file(
            self,
            replay_buffer_path: str,
            batch_size: int = 64,
            epochs: int = 1024,
            gradient_steps: int = 32,
            learning_rate: float = 0.0001,
            save_model_path: Optional[str] = f'model_{time.time_ns()}.torch',
            existing_model_path: str = None):
        self.train(
            self._load_replay_buffer_from_file(replay_buffer_path),
            batch_size=batch_size,
            epochs=epochs,
            gradient_steps=gradient_steps,
            learning_rate=learning_rate,
            save_model_path=save_model_path,
            existing_model_path=existing_model_path,
        )

    def _prepare_dataset(self, replay_buffer, rng_seed=123):
        X, y = self._load_dataset(replay_buffer)
        return train_test_split(
            X.astype(np.float32),
            y.astype(np.float32),
            test_size=0.05,
            shuffle=True,
            random_state=rng_seed,
        )

    def _retrieve_unnormalized_samples(self, replay_buffer):
        states = [s[0] for s in replay_buffer]
        actions = [s[1] for s in replay_buffer]
        X = states_actions_to_unnormalized_input(
            states,
            actions,
            self.num_discrete_actions
        )
        next_states = [s[3] for s in replay_buffer]
        y = np.array(next_states)
        return X, y

    def _lazy_load_model(self, replay_buffer, existing_model_path):
        if self.model is None:
            unnormalized_inputs, _ = self._retrieve_unnormalized_samples(replay_buffer)
            self.model = EnvironmentalRegressionModel(
                self.num_state_variables + self.num_discrete_actions,
                self.num_state_variables,
                list(np.mean(unnormalized_inputs, axis=0)),
                list(np.std(unnormalized_inputs, axis=0)),
                self.num_discrete_actions,
                model_path=existing_model_path,
            )

    def train(
            self,
            replay_buffer: replay_buffer_type,
            batch_size: int = 64,
            epochs: int = 1024,
            gradient_steps: int = 32,
            learning_rate=0.0001,
            save_model_path: Optional[str] = f'model_{time.time_ns()}.torch',
            existing_model_path: Optional[str] = None):
        self._lazy_load_model(replay_buffer, existing_model_path)
        optimizer = optim.Adam(self.model.parameters(), lr=learning_rate)
        X_train, X_test, y_train, y_test = self._prepare_dataset(replay_buffer)

        losses = []
        for _ in range(epochs):
            loss = self.train_batch(
                *self._sample_random_batch(X_train, y_train, batch_size),
                optimizer,
                gradient_steps=gradient_steps
            )
            losses.append(loss)
        
        self.evaluate(X_test, y_test, verbose=True)
        if save_model_path:
            torch.save(self.model.state_dict(), save_model_path)

    @staticmethod
    def _sample_random_batch(X, y, batch_size):
        random_indices = random.sample(range(len(X)), batch_size)
        return X[random_indices], y[random_indices]

    @staticmethod
    def _load_replay_buffer_from_file(file_name: str):
        with open(file_name, 'rb') as file:
            return pickle.load(file)

    def _load_dataset(self, replay_buffer) -> tuple[np.ndarray, np.ndarray]:
        X_unnormalized, y = self._retrieve_unnormalized_samples(replay_buffer)
        X = self.model.normalize_inputs(X_unnormalized)
        return X, y

    def evaluate(self, X_test, y_test, verbose=False):
        self.model.eval()
        with torch.no_grad():
            y_hat = self.model.forward(torch.from_numpy(X_test)).numpy()

            # we scale the output to [0...1] so all variables have equal impact
            # on the loss (independent of their range)
            min_max_scaler = MinMaxScaler()
            y_test = min_max_scaler.fit_transform(y_test)
            y_hat = min_max_scaler.transform(y_hat)

            abs_diff_loss = np.mean(np.sum(np.abs(y_hat - y_test), axis=1))
            summed_state_vars = np.mean(np.sum(y_test, axis=1))
            if verbose:
                print(f'Normalized Loss: {abs_diff_loss:.3f} | '
                      f'Normalized Cumulative Output: {summed_state_vars:.3f}.')
            return abs_diff_loss, summed_state_vars

    def train_batch(self, X, y, optimizer, gradient_steps=32, loss_fn=nn.MSELoss()):
        self.model.train()
        losses = []
        for _ in range(gradient_steps):
            y_hat = self.model.forward(torch.from_numpy(X))
            loss = loss_fn(y_hat, torch.from_numpy(y))
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            losses.append(float(loss))
        return losses


if __name__ == '__main__':
    trainer = EnvironmentalModelTrainer(20, 5)
    trainer.train_from_file(
        'replay_buffer_1634390324674640000.pkl',
        epochs=500_000,
        # existing_model_path='model_1634425623242916000.torch',
    )
