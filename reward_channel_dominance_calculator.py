import numpy as np


class RewardChannelDominanceCalculator:
    def __init__(
            self,
            action_semantics: tuple[str, ...],
            reward_channel_semantics: tuple[str, ...]) -> None:
        self.action_semantics = action_semantics
        self.reward_channel_semantics = reward_channel_semantics

    def get_channel_values(self, absolute_dominance_values):
        channel_values = []
        for rs in self.reward_channel_semantics:
            channel_values.append([])
            for ase in self.action_semantics:
                channel_values[-1].append(absolute_dominance_values[ase][rs])
        return channel_values

    def get_absolute_reward_channel_dominance(self, channel_values):
        absolute_channel_values = {}
        for i, ase in enumerate(self.action_semantics):
            absolute_channel_values[ase] = {}
            for j, rs in enumerate(self.reward_channel_semantics):
                absolute_channel_values[ase][rs] = float(channel_values[j][i])
        return absolute_channel_values

    def get_relative_reward_channel_dominance(self, channel_values):
        relative_channel_values = {}
        for i, ase in enumerate(self.action_semantics):
            relative_channel_values[ase] = {}
            for j, rs in enumerate(self.reward_channel_semantics):
                relative_channel_values[ase][rs] = float(channel_values[j][i] - np.min(channel_values[j]))
        return relative_channel_values
