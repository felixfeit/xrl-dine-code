import numpy as np


class MinimalSufficientExplanationExplainer:
    # Juozapaitis et al.
    def __init__(
            self,
            reward_channel_semantics: tuple[str, ...]) -> None:
        self.reward_channel_semantics = reward_channel_semantics

    def calculate_msx(
            self,
            action_1_channel_values: list[float],
            action_2_channel_values: list[float]):
        reasons = self._calculate_reasons(
            action_1_channel_values,
            action_2_channel_values
        )
        msx_positive, a1_is_better_action = self._calculate_positive_msx(reasons)
        msx_negative, _ = self._calculate_negative_msx(msx_positive, reasons)
        return msx_positive, msx_negative, a1_is_better_action

    def _calculate_reasons(
            self,
            action_1_channel_values: list[float],
            action_2_channel_values: list[float]):
        action_1_channel_values = np.array(action_1_channel_values)
        action_2_channel_values = np.array(action_2_channel_values)
        reason_vector = action_1_channel_values - action_2_channel_values
        return list(zip(self.reward_channel_semantics, reason_vector))

    def _calculate_positive_msx(self, reasons):
        absolute_disadvantage = abs(np.sum([r[1] for r in reasons if r[1] < 0]))
        sorted_advantages = sorted(
            reasons,
            key=lambda v: v[1],
            reverse=True
        )

        msx_positive = []
        outweighed = False
        for reward_semantic, advantage in sorted_advantages:
            current_msx_value = sum([v[1] for v in msx_positive])
            outweighed = current_msx_value > absolute_disadvantage
            if outweighed or advantage < 0:
                break

            msx_positive.append((reward_semantic, advantage))

        return msx_positive, outweighed
    
    def _calculate_negative_msx(self, msx_positive, reasons):
        just_insufficient_advantage = sum([v[1] for v in msx_positive[:-1]])
        sorted_disadvantages = sorted(
            reasons,
            key=lambda v: v[1]
        )

        msx_negative = []
        outweighed = False
        for reward_semantic, disadvantage in sorted_disadvantages:
            current_msx_value = sum([abs(v[1]) for v in msx_negative])
            outweighed = abs(current_msx_value) > just_insufficient_advantage
            if outweighed or disadvantage > 0:
                break

            msx_negative.append((reward_semantic, disadvantage))

        return msx_negative, outweighed


class RewardDecompositionNaturalLanguageExplainer:
    def __init__(
            self,
            observation_semantics: tuple[str, ...],
            action_semantics: tuple[str, ...],
            reward_channel_semantics: tuple[str, ...]) -> None:
        self.observation_semantics = observation_semantics
        self.action_semantics = action_semantics
        self.reward_channel_semantics = reward_channel_semantics
        self.msx_explainer = \
            MinimalSufficientExplanationExplainer(reward_channel_semantics)

    def _get_channel_values_of_action(self, action_values, action):
        # returns a list where each element represents the action value for
        # one reward channel
        return [channel_values[action] for channel_values in action_values]

    def get_natural_language_msx(
            self,
            channel_values,
            selected_action_index,
            alternative_action_index):
        selected_channel_values = self._get_channel_values_of_action(
            channel_values,
            selected_action_index
        )
        alternative_channel_values = self._get_channel_values_of_action(
            channel_values,
            alternative_action_index
        )

        positive_msx, negative_msx, is_better_action = \
            self.msx_explainer.calculate_msx(
                selected_channel_values,
                alternative_channel_values
            )

        if not is_better_action:
            positive_nl_msx = negative_nl_msx = \
                'The action was selected to explore the state space.'
        else:
            positive_nl_msx = self.get_positive_natural_language_msx(
                positive_msx,
                selected_action_index
            )
            negative_nl_msx = self.get_negative_natural_language_msx(
                negative_msx,
                selected_action_index
            )

        return positive_nl_msx, negative_nl_msx

    @staticmethod
    def _replace_last_occurrence(string, pattern, replace_with):
        return replace_with.join(string.rsplit(pattern, 1))

    def get_negative_natural_language_msx_goals(self, negative_msx):
        if len(negative_msx) == 0:
            return ''
        elif len(negative_msx) == 1:
            return f'<b><u>{negative_msx[0][0]}</u></b>'
        else:
            positive_msx_sum = sum([pm[1] for pm in negative_msx])
            goals = ''
            for reward_semantic, action_value in negative_msx:
                percentage = 100 * (action_value / positive_msx_sum)
                goals += f'<b><u>{reward_semantic}</u></b> ({percentage:.1f}%), '
            goals = goals[:-2]
            goals = self._replace_last_occurrence(goals, ',', ' and')
            return goals

    def get_negative_natural_language_msx(self, negative_msx, selected_action_index):
        if len(negative_msx) == 0:
            return ''
        
        negative_goals = self.get_negative_natural_language_msx_goals(negative_msx)
        return \
            f'Action <b><u>{self.action_semantics[selected_action_index]}</u></b> ' \
            f'was selected as its advantages outweigh the negative ' \
            f'impact on the goal{"s" if len(negative_msx) > 1 else ""} ' \
            f'{negative_goals}.'

    def get_positive_natural_language_msx_goals(self, positive_msx):
        if len(positive_msx) == 1:
            return f'<b><u>{positive_msx[0][0]}</u></b>'
        else:  # len(positive_msx) > 1
            positive_msx_sum = sum([pm[1] for pm in positive_msx])
            goals = ''
            for reward_semantic, action_value in positive_msx:
                percentage = 100 * (action_value / positive_msx_sum)
                goals += f'<b><u>{reward_semantic}</u></b> ({percentage:.1f}%), '
            goals = goals[:-2]
            goals = self._replace_last_occurrence(goals, ',', ' and')
            return goals

    def get_positive_natural_language_msx(self, positive_msx, selected_action_index):
        positive_goals = self.get_negative_natural_language_msx_goals(positive_msx)
        return \
            f'Action <b><u>{self.action_semantics[selected_action_index]}</u></b> ' \
            f'was selected as its advantages outweigh the negative ' \
            f'impact on the goal{"s" if len(positive_msx) > 1 else ""} ' \
            f'{positive_goals}.'

    def get_constrastive_important_action_explanation(
            self,
            channel_values,
            selected_action_index,
            alternative_reward_channel_index,
            alternative_important_action_index):
        selected_channel_values = self._get_channel_values_of_action(
            channel_values,
            selected_action_index
        )
        alternative_channel_values = self._get_channel_values_of_action(
            channel_values,
            alternative_important_action_index
        )

        positive_msx, _, is_better_action = \
            self.msx_explainer.calculate_msx(
                selected_channel_values,
                alternative_channel_values
            )

        if not is_better_action:
            return 'The action was selected to explore the state space.'

        positive_goals = self.get_positive_natural_language_msx_goals(positive_msx)

        # TODO: maybe calculate percentages differently (as "contrastive percentages")
        return \
            f'To achieve the goal ' \
            f'<b><u>{self.reward_channel_semantics[alternative_reward_channel_index]}</u></b>, it ' \
            f'would currently be most important to choose ' \
            f'<b><u>{self.action_semantics[alternative_important_action_index]}</u></b>. ' \
            f'However, taking into account all the contrastive target dimensions, ' \
            f'I have chosen the action <b><u>{self.action_semantics[selected_action_index]}</u></b>.\n\n' \
            f'More specifically, compared to the action ' \
            f'<b><u>{self.action_semantics[alternative_important_action_index]}</u></b>, the action ' \
            f'<b><u>{self.action_semantics[selected_action_index]}</u></b> has ' \
            f'a higher positive impact on ' \
            f'the goal{"s" if len(positive_msx) > 1 else ""} {positive_goals}.'

    def get_contrastive_explanation(
            self,
            channel_values,
            selected_action_index,
            alternative_action_index):
        selected_channel_values = self._get_channel_values_of_action(
            channel_values,
            selected_action_index
        )
        alternative_channel_values = self._get_channel_values_of_action(
            channel_values,
            alternative_action_index
        )

        positive_msx, negative_msx, is_better_action = \
            self.msx_explainer.calculate_msx(
                selected_channel_values,
                alternative_channel_values
            )

        if not is_better_action:
            return 'The action was selected to explore the state space.'

        positive_goals = self.get_positive_natural_language_msx_goals(positive_msx)
        negative_goals = self.get_negative_natural_language_msx_goals(negative_msx)

        if negative_goals:
            return \
                f'To achieve the goal{"s" if len(negative_msx) > 1 else ""} ' \
                f'{negative_goals}, action ' \
                f'<b><u>{self.action_semantics[alternative_action_index]}</u></b> should ' \
                f'actually be chosen. However, it is currently more important to ' \
                f'choose action <b><u>{self.action_semantics[alternative_action_index]}</u></b> ' \
                f'to have a positive effect on ' \
                f'goal{"s" if len(positive_msx) > 1 else ""} {positive_goals}.'

        else:
            return \
                f'Action <b><u>{self.action_semantics[selected_action_index]}</u></b> ' \
                f'was in relation to all goals superior to ' \
                f'<b><u>{self.action_semantics[alternative_action_index]}</u></b>. ' \
                f'However, the main reason was its impact on the ' \
                f'goal{"s" if len(positive_msx) > 1 else ""} {positive_goals}.'
