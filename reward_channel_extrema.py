import numpy as np

from decomposed_reward_deep_q_learning import DecomposedRewardDeepQLearningTrainer
from environmental_model import EnvironmentalModelTrainer

DQNSubagent = DecomposedRewardDeepQLearningTrainer.DoubleDQNSubAgent


class RewardChannelExtremaCalculator:
    def __init__(
            self,
            num_actions: int,
            environmental_model_trainer: EnvironmentalModelTrainer,
            action_value_models: list[DQNSubagent]) -> None:
        self.num_actions = num_actions
        self._environmental_model_trainer: EnvironmentalModelTrainer = environmental_model_trainer
        self._action_value_models: list[DQNSubagent] = action_value_models

    def infer_state_value(self, state: np.ndarray, reward_channel_id: int):
        action_value_model = self._action_value_models[reward_channel_id]
        # approximate state value by returning the max action-value
        # proposed in "Interestingness Elements for Explainable Reinforcement
        # Learning: Understanding Agents Capabilities and Limitations"
        return float(np.max(action_value_model.get_action_values(state)))

    def _infer_next_state(self, state, action):
        assert self._environmental_model_trainer.model is not None, \
            'The model has not been initialized.'
        return self._environmental_model_trainer.model.infer_next_state(state, action)

    def _infer_next_state_value(self, state, action, reward_channel_id):
        next_state = self._infer_next_state(state, action)
        return self.infer_state_value(next_state, reward_channel_id)

    def _infer_next_state_values(self, state, reward_channel_id):
        next_state_values = []
        for action in range(self.num_actions):
            next_state_values.append(
                self._infer_next_state_value(state, action, reward_channel_id)
            )
        return next_state_values

    def is_local_maximum(self, state, reward_channel_id, threshold=0.1):
        current_state_value = self.infer_state_value(state, reward_channel_id)
        barrier = self.get_predicted_max_next_state_value(state, reward_channel_id) * (1 + threshold)
        return current_state_value > barrier

    def is_local_minimum(self, state, reward_channel_id, threshold=0.1):
        current_state_value = self.infer_state_value(state, reward_channel_id)
        return current_state_value * (1 + threshold) < self.get_predicted_min_next_state_value(state, reward_channel_id)

    def get_predicted_min_next_state_value(self, state, reward_channel_id):
        next_state_values = self._infer_next_state_values(state, reward_channel_id)
        return float(min(next_state_values))

    def get_predicted_max_next_state_value(self, state, reward_channel_id):
        next_state_values = self._infer_next_state_values(state, reward_channel_id)
        return float(max(next_state_values))
