import json
from pathlib import Path
from time import sleep
from typing import Sequence, Optional

import numpy as np

from decomposed_reward_deep_q_learning import DecomposedRewardDeepQLearningTrainer
from environmental_model import EnvironmentalModelTrainer
from explanation import Explanation
from important_interactions import EvennnessAnalyzer
from reward_channel_dominance_calculator import RewardChannelDominanceCalculator
from reward_channel_extrema import RewardChannelExtremaCalculator
from reward_decomposition_explanations import RewardDecompositionNaturalLanguageExplainer

DQNSubagent = DecomposedRewardDeepQLearningTrainer.DoubleDQNSubAgent


class DecomposedInterestingnessElementsEngine:
    class Configuration:
        def __init__(
                self,
                train_model_interval: int = 64,
                train_model_starts: int = 500_000,
                train_model_initial_epochs: int = 200_000,
                important_interaction_threshold: float = 0.8) -> None:
            self.train_model_interval = train_model_interval
            self.train_model_starts = train_model_starts
            self.train_model_initial_epochs = train_model_initial_epochs
            self.important_interaction_threshold = important_interaction_threshold

    def __init__(
            self,
            observation_semantics: tuple[str, ...],
            action_semantics: tuple[str, ...],
            reward_channel_semantics: tuple[str, ...],
            replay_buffer_reference: Sequence,
            action_value_models: list[DQNSubagent],
            configuration: Configuration = Configuration(),
            normalize_state_variables: bool = True,
            explanations_running_out_file: Optional[str] = './explanation_data/explanations.json',
            num_sleep_seconds_per_action: int = 0,
            start_sleep_after_interaction: int = 0) -> None:
        self.observation_semantics: tuple[str, ...] = observation_semantics
        self.action_semantics: tuple[str, ...] = action_semantics
        self.reward_channel_semantics: tuple[str, ...] = reward_channel_semantics
        self.action_value_models: list[DQNSubagent] = action_value_models
        self.conf: self.Configuration = configuration
        self.normalize_state_variables = normalize_state_variables
        self.explanations_out_file = explanations_running_out_file
        Path(self.explanations_out_file).unlink(missing_ok=True)  # delete if exists
        self.num_sleep_seconds_per_action = num_sleep_seconds_per_action
        self.start_sleep_after_interaction = start_sleep_after_interaction
        self._replay_buffer: Sequence = replay_buffer_reference
        self._interaction_count: int = 0

        self.reward_decomposition_explainer = RewardDecompositionNaturalLanguageExplainer(
            observation_semantics,
            action_semantics,
            reward_channel_semantics
        )
        self.important_interactions_explainer = EvennnessAnalyzer
        self.reward_channel_dominance_explainer = RewardChannelDominanceCalculator(
            action_semantics,
            reward_channel_semantics
        )
        self._environmental_model_trainer = EnvironmentalModelTrainer(
            len(observation_semantics),
            len(action_semantics)
        )
        self.reward_channel_extrema_explainer = RewardChannelExtremaCalculator(
            len(action_semantics),
            self._environmental_model_trainer,
            action_value_models,
        )

        if self.conf.train_model_initial_epochs > 0:
            self._environmental_model_trainer.train(
                self._replay_buffer,
                epochs=self.conf.train_model_initial_epochs,
                save_model_path=None
            )

    def _get_differing_important_actions(self, channel_values) -> list[tuple[int, int, int, int]]:
        threshold = self.conf.important_interaction_threshold
        get_important_interaction = \
            self.important_interactions_explainer.get_strongest_top_outlier_index
        important_actions = [get_important_interaction(av, threshold)
                             for av in channel_values]
        differing_important_actions = []
        for i, ia1 in enumerate(important_actions[:-1]):
            if ia1 is not None:
                for j, ia2 in enumerate(important_actions[i + 1:]):
                    if ia2 is not None and ia1 != ia2:
                        differing_important_actions.append((i, ia1, j, ia2))

        return differing_important_actions

    def _get_contrastive_important_actions(self, channel_values, selected_action) -> list[tuple[int, int]]:
        threshold = self.conf.important_interaction_threshold
        get_important_interaction = \
            self.important_interactions_explainer.get_strongest_top_outlier_index
        important_actions = [get_important_interaction(av, threshold)
                             for av in channel_values]
        contrastive_important_actions = []
        for i, ia in enumerate(important_actions):
            if ia is not None and ia != selected_action:
                contrastive_important_actions.append((i, ia))

        return contrastive_important_actions
    
    def _get_contrastive_important_actions_explanations(self, channel_values, selected_action):
        differing_important_actions = self._get_contrastive_important_actions(
            channel_values,
            selected_action
        )
        contrastive_important_actions_explanations = []
        for dia in differing_important_actions:
            ce = self.reward_decomposition_explainer.get_constrastive_important_action_explanation(
                channel_values,
                selected_action,
                dia[0],
                dia[1]
            )
            contrastive_important_actions_explanations.append((
                self.reward_channel_semantics[dia[0]],
                self.action_semantics[dia[1]],
                ce
            )) 
        return contrastive_important_actions_explanations

    def report_new_interaction(
            self,
            state: list[float],
            decomposed_reward: list[float],
            selected_action: int) -> None:
        if self._interaction_count > self.conf.train_model_starts and \
           self._interaction_count % self.conf.train_model_interval == 0:
            self._environmental_model_trainer.train(self._replay_buffer)

        channel_values = \
            [avm.get_action_values(state) for avm in self.action_value_models]
        is_greedy_action = \
            bool(np.argmax(np.sum(channel_values, axis=0)) == selected_action)
       
        contrastive_important_actions_explanations = \
            self._get_contrastive_important_actions_explanations(channel_values, selected_action)

        current_state_value = {}
        min_next_state_value = {}
        max_next_state_value = {}
        normalized_state_representation = {}
        state_representation = {ose: state[i] for i, ose in enumerate(self.observation_semantics)}
        if self._environmental_model_trainer.model is not None:  # initialized?
            get_current = self.reward_channel_extrema_explainer.infer_state_value
            get_min = self.reward_channel_extrema_explainer.get_predicted_min_next_state_value
            get_max = self.reward_channel_extrema_explainer.get_predicted_max_next_state_value
            for i, semantic in enumerate(self.reward_channel_semantics):
                current_state_value[semantic] = get_current(state, i)
                min_next_state_value[semantic] = get_min(state, i)
                max_next_state_value[semantic] = get_max(state, i)

            normalized_state = self._environmental_model_trainer.model.normalize_state(state)
            normalized_state_representation = {ose: float(normalized_state[i]) for i, ose in enumerate(self.observation_semantics)}

        explanation: Explanation = Explanation(
            timestep=self._interaction_count,
            last_rewards={rs: decomposed_reward[i] for i, rs in enumerate(self.reward_channel_semantics)},
            current_state=state_representation,
            current_normalized_state=normalized_state_representation,
            current_selected_action=self.action_semantics[selected_action],
            is_greedy_action=is_greedy_action,
            absolute_reward_channel_dominance= \
                self.reward_channel_dominance_explainer.get_absolute_reward_channel_dominance(channel_values),
            relative_reward_channel_dominance= \
                self.reward_channel_dominance_explainer.get_relative_reward_channel_dominance(channel_values),
            current_state_value=current_state_value,
            min_next_state_value=min_next_state_value,
            max_next_state_value=max_next_state_value,
            contrastive_important_actions_explanations=contrastive_important_actions_explanations
        )
        self._append_explanation_to_file(explanation)

        self._interaction_count += 1
        if self._interaction_count > self.start_sleep_after_interaction:
            sleep(self.num_sleep_seconds_per_action)

    def _append_explanation_to_file(self, explanation: Explanation, explanations_to_keep: int = 500):
        path = Path(self.explanations_out_file)
        path.parent.mkdir(parents=True, exist_ok=True)
        existing_data = []
        if path.is_file():
            with open(path, 'r') as f:
                existing_data = json.load(f)
        
        existing_data.append(explanation.to_dict())
        with open(path, 'w', encoding='utf-8') as f:
            json.dump(existing_data[-explanations_to_keep:], f, ensure_ascii=False, indent=4)

    def save_checkpoint(self, checkpoint_out_file: str):
        checkpoint_path = Path(checkpoint_out_file)
        explanations_path = Path(self.explanations_out_file)
        if explanations_path.is_file():
            checkpoint_path.parent.mkdir(parents=True, exist_ok=True)
            with open(explanations_path, 'r') as f:
                existing_data = json.load(f)

            with open(checkpoint_path, 'w', encoding='utf-8') as f:
                json.dump(existing_data, f, ensure_ascii=False, indent=4)
